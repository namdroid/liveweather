package com.namdroid.liveweather

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.google.gson.Gson
import com.namdroid.liveweather.database.AppDatabase
import com.namdroid.liveweather.database.Dao.CityDao
import com.namdroid.liveweather.database.Dao.WeatherDao
import com.namdroid.liveweather.model.WeatherData
import com.namdroid.liveweather.model.WeatherResponse
import com.namdroid.liveweather.repository.AppRepository
import com.namdroid.liveweather.rest.OpenWeatherService
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.timeout
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset

@RunWith(AndroidJUnit4::class)
class AppRepositoryTest {

    @Rule
    @JvmField
    var instantExecutor = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<WeatherData>
    private lateinit var mockWebServer: MockWebServer
    private lateinit var database: AppDatabase
    private lateinit var cityDao: CityDao
    private lateinit var weatherDao: WeatherDao
    private lateinit var repository: AppRepository
    private lateinit var retrofit: Retrofit
    private lateinit var openWeatherService: OpenWeatherService

    companion object {
        val TIMEOUT = 1000L
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).allowMainThreadQueries().build()
        weatherDao = database.weatherDao()
        cityDao = database.cityDao()

        mockWebServer = MockWebServer()
        mockWebServer.start()

        retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/").toString())
                .client(OkHttpClient())
                .build()
        openWeatherService = retrofit.create<OpenWeatherService>(OpenWeatherService::class.java)
        repository = AppRepository(openWeatherService,context, weatherDao,cityDao)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getValidFieldsTest() {
        val weatherJson = this.javaClass.classLoader.getResource("weather.json").readText(Charset.forName("UTF-8"))
        mockWebServer.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody(weatherJson))

        val weatherResponse = Gson().fromJson(weatherJson, WeatherResponse::class.java)
        val weatherData:WeatherData = WeatherData.mapFrom(weatherResponse)

        //when
        repository.getCityWeather(weatherData.cityId!!).observeForever(observer)
        //then
        verify(observer, timeout(TIMEOUT)).onChanged(ArgumentMatchers.refEq(weatherData))
    }
}