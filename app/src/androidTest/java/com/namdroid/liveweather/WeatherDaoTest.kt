package com.namdroid.liveweather

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.google.gson.Gson
import com.namdroid.liveweather.database.AppDatabase
import com.namdroid.liveweather.database.Dao.WeatherDao
import com.namdroid.liveweather.model.WeatherData
import com.namdroid.liveweather.model.WeatherResponse
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.nio.charset.Charset


@RunWith(AndroidJUnit4::class)
class WeatherDaoTest {

    @Rule
    @JvmField
    var instantExecutor = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<WeatherData>

    private lateinit var database: AppDatabase
    private lateinit var weatherDao: WeatherDao

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .allowMainThreadQueries().build()
        weatherDao = database.weatherDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertWeather() {
        val weatherJson = this.javaClass.classLoader.getResource("weather.json").readText(Charset.forName("UTF-8"))

        val weatherResponse = Gson().fromJson(weatherJson, WeatherResponse::class.java)
        val weatherData:WeatherData = WeatherData.mapFrom(weatherResponse)

        weatherDao.insert(weatherData)

        weatherDao.getWeatherByCityId(weatherData.cityId!!).observeForever(observer)

        verify(observer).onChanged(weatherData)
    }
}