package com.namdroid.liveweather.rest

import com.namdroid.liveweather.model.WeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by namdr on 12.02.2018.
 */
interface OpenWeatherService {

    // get weather by city
    @GET("weather")
    fun getCityWeather(@Query("id") cityId: Int, @Query("APPID") appId: String): Single<WeatherResponse>
}