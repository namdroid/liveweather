package com.namdroid.liveweather.util

import android.content.Context
import com.google.gson.stream.JsonReader
import com.namdroid.liveweather.model.City
import com.namdroid.liveweather.model.Coord
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import java.io.*
import java.util.*
import android.net.ConnectivityManager



/**
 * Created by namdr on 14.02.2018.
 */
object Utils:AnkoLogger {

     fun loadCitiesFromAsset(context: Context): List<City>? {
        val cities = LinkedList<City>()

        try {
            val inputStream = context.assets.open("city_list.json")
            try {
                val reader = JsonReader(InputStreamReader(inputStream, "UTF-8"))
                reader.beginArray()

                while (reader.hasNext()) {
                    val city = readCity(reader)
                    cities.add(city)
                }
                reader.endArray()
                reader.close()
            }catch (ex: UnsupportedEncodingException) {
                error("parse json: " + ex.message)
            }

        } catch (ex: IOException) {
            error(ex.message)
            return null
        }

        return cities
    }


    @Throws(IOException::class)
    fun readCity(reader: JsonReader): City {
        var id: Int = -1
        var name: String? = null
        var coord: Coord? = null
        var country: String? = null

        reader.beginObject()
        while (reader.hasNext()) {
            val nextName = reader.nextName()
            when (nextName) {
                "id" -> id = reader.nextInt()
                "name" -> name = reader.nextString()
                "country" -> country = reader.nextString()
                "coord" -> coord = readCoord(reader)
                else -> reader.skipValue()
            }
        }
        reader.endObject()
        return City(id, name, country, coord)
    }


    @Throws(IOException::class)
    private fun readCoord(reader: JsonReader): Coord {
        var lat: Double? = null
        var lon: Double? = null

        reader.beginObject()
        while (reader.hasNext()) {
            val name = reader.nextName()
            when (name) {
                "lat" -> lat = reader.nextDouble()
                "lon" -> lon =  reader.nextDouble()
                else -> reader.skipValue()
            }
        }
        reader.endObject()
        return Coord(lat, lon)
    }

    fun isNetworkAvailable(context: Context):Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}

