package com.namdroid.liveweather.ui.main

import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import com.namdroid.liveweather.R
import com.namdroid.liveweather.ui.listfragment.CityListFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.progressDialog
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel
    private var dialog: ProgressDialog? = null
    private lateinit var disposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        dialog = this.progressDialog(message = "Please wait a bit…", title = "Fetching cities")
        dialog?.max = viewModel.getMaxCity()
        dialog?.setCancelable(false)
        dialog?.hide()

        disposable = CompositeDisposable()

        val cityListFragment: Fragment? = supportFragmentManager.findFragmentByTag("cityfragment")

        if (cityListFragment == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, CityListFragment(),"cityfragment")
                    .commit()
        }

        viewModel.getCityCount().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            if (it < viewModel.getMaxCity()) {
                                loadCities()
                            }
                        }, {}
                ).addTo(disposable)
    }

    private fun loadCities() {
        dialog?.show()

        viewModel.loadLocations().observe(this, Observer<Progress> {
            it?.let {
                dialog?.progress = it.totalRead
                if (it.finished) {
                    dialog?.dismiss()
                }
            }
        })
    }

    override fun onStop() {
        super.onStop()
        dialog?.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }


    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector
}
