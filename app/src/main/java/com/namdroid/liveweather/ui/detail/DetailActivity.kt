package com.namdroid.liveweather.ui.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.namdroid.liveweather.R
import com.namdroid.liveweather.model.WeatherData
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_city_detail.*
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject
import java.text.SimpleDateFormat


class DetailActivity : AppCompatActivity(), AnkoLogger {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_detail)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        val cityID = intent.getIntExtra(CITY_ID,0)

        viewModel.getCityWeather(cityID).observe(this, Observer {
            it?.let { showWeather(it) }
        })
    }


    fun showWeather(data:WeatherData) {
        city_name.text = data.name
        temp.text = String.format(getString(R.string.celsius), data.temp!!.minus(273.15))
        description.text = data.description
        humidity.text = String.format(getString(R.string.humidity),data.humidity)
        val df = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val formattedDate = df.format(data.dt!!.times(1000))

        time.text = formattedDate

        Glide.with(this).load(data.icon).into(w_icon)
    }

    companion object {
        private val CITY_ID = "CITY_ID"
        fun launchActivity(context: Context,cityId:Int) {
            val intent = Intent(context,DetailActivity::class.java)
            intent.putExtra(CITY_ID,cityId)
            context.startActivity(intent)
        }
    }
}
