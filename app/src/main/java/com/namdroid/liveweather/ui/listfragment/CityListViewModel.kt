package com.namdroid.liveweather.ui.listfragment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.namdroid.liveweather.model.City
import com.namdroid.liveweather.repository.AppRepository
import javax.inject.Inject


class CityListViewModel @Inject constructor(private val appRepository: AppRepository) : ViewModel() {


    fun getCities(name:String): LiveData<List<City>> {
        return appRepository.getCitiesByName(name)
    }
}