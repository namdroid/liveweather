package com.namdroid.liveweather.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.namdroid.liveweather.repository.AppRepository
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: AppRepository) : ViewModel(),AnkoLogger {

    private val progress = MutableLiveData<Progress>()
    private var loadCitiesDispose:Disposable? = null
    private var compositeDisposable: CompositeDisposable

    init {
        compositeDisposable = CompositeDisposable()
    }



    fun loadLocations():LiveData<Progress> {

        if (loadCitiesDispose == null || loadCitiesDispose!!.isDisposed) {
            loadCitiesDispose = repository.loadCities().subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .doOnNext { progress.postValue(it) }
                    .subscribe()

            compositeDisposable.add(loadCitiesDispose!!)
        }

        return progress
    }

    fun getCityCount(): Single<Int> {
        return repository.getTotalCity()
    }

    fun getMaxCity() = repository.getMaxCity()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}