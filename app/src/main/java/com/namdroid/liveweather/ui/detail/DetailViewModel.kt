package com.namdroid.liveweather.ui.detail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.namdroid.liveweather.model.WeatherData
import com.namdroid.liveweather.repository.AppRepository
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val repo: AppRepository) : ViewModel() {

    fun getCityWeather(cityId:Int): LiveData<WeatherData> {
        return repo.getCityWeather(cityId)
    }
}