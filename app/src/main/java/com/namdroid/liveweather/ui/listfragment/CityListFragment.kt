package com.namdroid.liveweather.ui.listfragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.namdroid.liveweather.databinding.CityListFragmentBinding
import com.namdroid.liveweather.di.Injectable
import com.namdroid.liveweather.di.ViewModelFactory
import com.namdroid.liveweather.model.City
import com.namdroid.liveweather.ui.detail.DetailActivity
import com.namdroid.liveweather.ui.listfragment.CityListAdapter.ViewItemClickListener
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject
import android.view.inputmethod.InputMethodManager


/**
 * Created by namdr on 12.02.2018.
 */

class CityListFragment : Fragment(), Injectable, AnkoLogger {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: CityListViewModel
    lateinit var adapter: CityListAdapter
    lateinit var binding: CityListFragmentBinding
    var searchText:CharSequence = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = CityListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        retainInstance = true
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CityListViewModel::class.java)
        this.adapter = CityListAdapter(clickListener)

        binding.cityList.adapter = this.adapter
        binding.viewModel = viewModel

        binding.buttonSearch.setOnClickListener {
            searchText = binding.searchText.text
            searchCity(searchText.toString())
        }

        binding.searchText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                searchText = binding.searchText.text
                searchCity(searchText.toString())
            }
            false
        }

        if (savedInstanceState != null) {
            searchText = savedInstanceState.getCharSequence("searchtext")
            searchCity(searchText.toString())
        }

    }

    private fun searchCity(text:String) {

        if (text.isEmpty()) {
            return
        }
        hideKeyBoard(binding.searchText)

        viewModel.getCities(text).observe(this, Observer {
            it?.let {
                if (it.isEmpty()) {
                    Toast.makeText(context, "No cities found", Toast.LENGTH_SHORT).show()
                } else {
                    adapter.setItems(it)
                }
            }
        })
    }

    private val clickListener: ViewItemClickListener = object : ViewItemClickListener {
        override fun onClick(city: City) {
            DetailActivity.launchActivity(activity, city.id)
        }
    }

    private fun hideKeyBoard(view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putCharSequence("searchtext", searchText)
        super.onSaveInstanceState(outState)
    }
}