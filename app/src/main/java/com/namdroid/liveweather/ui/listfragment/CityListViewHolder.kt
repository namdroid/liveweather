package com.namdroid.liveweather.ui.listfragment

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import com.namdroid.liveweather.BR
import com.namdroid.liveweather.model.City

class CityListViewHolder constructor(private val binding: ViewDataBinding, private val clickListener: CityListAdapter.ViewItemClickListener?) : RecyclerView.ViewHolder(binding.root) {

    fun bind(city: City) {
        binding.setVariable(BR.city, city)
        binding.executePendingBindings()

        binding.root.setOnClickListener { _ ->
            clickListener?.onClick(city)
        }
    }

}
