package com.namdroid.liveweather.ui.listfragment

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.namdroid.liveweather.R
import com.namdroid.liveweather.model.City

class CityListAdapter(clickListener: ViewItemClickListener) : RecyclerView.Adapter<CityListViewHolder>() {

    interface ViewItemClickListener {
        fun onClick(city:City)
    }

    private var cities: List<City>
    private var itemClickListener:ViewItemClickListener? = null

    init {
        cities = emptyList()
        itemClickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityListViewHolder {
        val viewDataBinding: ViewDataBinding? = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.city_item, parent, false)
        return CityListViewHolder(viewDataBinding!!,this.itemClickListener)
    }

    override fun onBindViewHolder(holder: CityListViewHolder, position: Int) {
        val city = cities[position]
        holder.bind(city)
    }

    override fun getItemCount() = cities.size

    fun setItems(items: List<City>) {
        this.cities = items
        notifyDataSetChanged()
    }

}
