package com.namdroid.liveweather.ui.main

/**
 * Created by namdr on 14.02.2018.
 */
class Progress(val totalRead: Int, val contentLength: Int, val finished: Boolean) {

    override fun toString(): String {
        return totalRead.toString() + "/" + contentLength + "(" + totalRead * 100 / contentLength + " %)"
    }
}