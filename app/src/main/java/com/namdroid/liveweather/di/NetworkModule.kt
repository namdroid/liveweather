package com.namdroid.liveweather.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.namdroid.liveweather.BuildConfig
import com.namdroid.liveweather.rest.OpenWeatherService

import java.net.ConnectException
import java.util.concurrent.TimeUnit

import dagger.Module
import dagger.Provides
import okhttp3.*
import javax.inject.Singleton

import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


@Module
internal class NetworkModule {

    @Provides
    fun provideRetrofit(
            gson: Gson,
            okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_OPEN_WEATHER_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }


    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(): OkHttpClient {

        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.readTimeout(10, TimeUnit.SECONDS)
        httpClientBuilder.connectTimeout(10, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(loggingInterceptor)
            httpClientBuilder.addNetworkInterceptor(StethoInterceptor())
        }
        httpClientBuilder.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            // Request customization: add request headers
            val requestBuilder = original.newBuilder()
                    .method(original.method(), original.body())

            val request = requestBuilder.build()

            try {
                return@Interceptor chain.proceed(request)
            } catch (e: ConnectException) {
                e.printStackTrace()
                throw e
            }
        })

        return httpClientBuilder.build()
    }



    @Provides
    @Singleton
    fun provideOpenWeatherService(retrofit : Retrofit): OpenWeatherService {
        return retrofit.create(OpenWeatherService::class.java)
    }
}