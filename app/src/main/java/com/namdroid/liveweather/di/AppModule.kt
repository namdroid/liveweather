package com.namdroid.liveweather.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.namdroid.liveweather.WeatherApplication
import com.namdroid.liveweather.database.AppDatabase
import com.namdroid.liveweather.database.Dao.CityDao
import com.namdroid.liveweather.database.Dao.WeatherDao
import com.namdroid.liveweather.database.DbMigration.MIGRATION_1_2


import javax.inject.Singleton

import dagger.Module
import dagger.Provides


@Module(includes = [ViewModelModule::class,NetworkModule::class])
class AppModule {


    @Provides
    @Singleton
    fun provideContext(application: WeatherApplication): Context = application


    @Provides
    @Singleton
    fun providesDatabaseReference(): DatabaseReference {
        val instance =  FirebaseDatabase.getInstance()
        instance.setPersistenceEnabled(false)
        instance.reference.keepSynced(false)

        return instance.reference
    }


    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
                    .addMigrations(MIGRATION_1_2).build()

    @Provides
    @Singleton
    fun provideCityDao(appDatabase: AppDatabase): CityDao = appDatabase.cityDao()

    @Provides
    @Singleton
    fun provideWeatherDao(appDatabase: AppDatabase): WeatherDao = appDatabase.weatherDao()
}
