package com.namdroid.liveweather.di

import com.namdroid.liveweather.WeatherApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    AndroidSupportInjectionModule::class,
    ActivityBuilderModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(app: WeatherApplication): Builder
        fun build(): AppComponent
    }

    fun inject(app: WeatherApplication)
}
