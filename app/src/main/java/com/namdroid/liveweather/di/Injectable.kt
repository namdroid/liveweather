package com.namdroid.liveweather.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable {
    // Empty on purpose
}