package com.namdroid.liveweather.di

import com.namdroid.liveweather.ui.listfragment.CityListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeCityListFragment(): CityListFragment
}