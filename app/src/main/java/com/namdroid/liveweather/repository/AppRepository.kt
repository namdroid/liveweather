package com.namdroid.liveweather.repository

/**
 * Created by namdr on 12.02.2018.
 */

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.google.gson.stream.JsonReader
import com.namdroid.liveweather.BuildConfig
import com.namdroid.liveweather.database.Dao.CityDao
import com.namdroid.liveweather.database.Dao.WeatherDao
import com.namdroid.liveweather.model.City
import com.namdroid.liveweather.model.WeatherData
import com.namdroid.liveweather.rest.OpenWeatherService
import com.namdroid.liveweather.ui.main.Progress
import com.namdroid.liveweather.util.Utils
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
open class AppRepository @Inject constructor(private val weatherService: OpenWeatherService,
                                             private val appConText: Context,
                                             private val weatherDao: WeatherDao,
                                             private val cityDao: CityDao) : AnkoLogger {

    private val totalLength: Int = 100000

    fun getCitiesByName(cityName: String): LiveData<List<City>> {
        return cityDao.getCity(cityName)
    }

    fun getTotalCity(): Single<Int> {
        return Single.fromCallable {
            cityDao.countCites()
        }
    }

    fun getMaxCity(): Int {
        return totalLength
    }

    // this call will take long time
    fun getCitiesByNameFromJson(cityName: String): LiveData<List<City>> {
        val cities = MutableLiveData<List<City>>()

        Completable.fromCallable {
            val citiesTmp = ArrayList<City>()
            val cityList = Utils.loadCitiesFromAsset(appConText)
            cityList!!.forEach {
                if (it.name!!.toLowerCase().contains(cityName.toLowerCase())) {
                    citiesTmp.add(it)
                }
            }
            cities.postValue(citiesTmp)
        }.subscribeOn(Schedulers.io()).subscribe()

        return cities
    }


    fun getCityWeather(cityId: Int): LiveData<WeatherData> {

        weatherService.getCityWeather(cityId, BuildConfig.OPEN_WEATHER_API_KEY)
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    if (response.cod == 200) {
                        val weatherData = WeatherData.mapFrom(response)
                        weatherDao.insert(weatherData)
                    }
                }, { e -> e?.let { error { e.message } } })


        return weatherDao.getWeatherByCityId(cityId)
    }


    fun loadCities(): Observable<Progress> {
        return Observable.create { emitter ->
            try {
                val inputStream = appConText.assets.open("city_list.json")
                val cities = LinkedList<City>()
                try {
                    val reader = JsonReader(InputStreamReader(inputStream, "UTF-8"))
                    var count = 0
                    reader.beginArray()
                    while (reader.hasNext() && count < totalLength) {
                        count++
                        val city = Utils.readCity(reader)
                        val progress = Progress(count, totalLength, false)
                        emitter.onNext(progress)
                        // info { progress.toString() }
                        cities.add(city)
                    }
                    cityDao.insertAll(cities)
                    emitter.onNext(Progress(count, totalLength, true))
                    info { "total cities in db : " + cityDao.countCites() }
                    // reader.endArray()
                    reader.close()
                    emitter.onComplete()
                } catch (ex: UnsupportedEncodingException) {
                    error("parse json: " + ex.message)
                    emitter.onError(Throwable(ex.message))
                }

            } catch (ex: IOException) {
                error(ex.message)
                emitter.onError(Throwable(ex.message))
            }
        }
    }

}