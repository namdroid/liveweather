package com.namdroid.liveweather.database.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.namdroid.liveweather.model.WeatherData

/**
 * Created by namdr on 15.02.2018.
 */
@Dao
interface WeatherDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE )
    fun insert( w: WeatherData )

    @Update
    fun update( w: WeatherData)

    @Query("SELECT * FROM weather_data WHERE cityId LIKE :cityId LIMIT 1")
    fun getWeatherByCityId(cityId: Int ): LiveData<WeatherData>
}