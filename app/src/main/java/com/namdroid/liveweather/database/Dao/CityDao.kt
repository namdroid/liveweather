package com.namdroid.liveweather.database.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.namdroid.liveweather.model.City

/**
 * Created by namdr on 14.02.2018.
 */
@Dao
interface CityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(city: City):Long

    @Update
    fun update(city: City)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<City>)

    @Query("SELECT * FROM cities")
    fun getAll(): LiveData<List<City>>

    @Query("SELECT * FROM cities WHERE name LIKE '%' || :cityName || '%'")
    fun getCity(cityName: String): LiveData<List<City>>

    @Query("SELECT COUNT(*) from cities")
    fun countCites(): Int
}