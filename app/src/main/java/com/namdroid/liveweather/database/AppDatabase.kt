package com.namdroid.liveweather.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.namdroid.liveweather.database.Dao.CityDao
import com.namdroid.liveweather.database.Dao.WeatherDao
import com.namdroid.liveweather.model.City
import com.namdroid.liveweather.model.WeatherData

/**
 * Created by namdr on 14.02.2018.
 */
@Database(entities = [(City::class), (WeatherData::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cityDao(): CityDao
    abstract fun weatherDao(): WeatherDao

    companion object {
        val DB_NAME = "liveweather"
    }
}