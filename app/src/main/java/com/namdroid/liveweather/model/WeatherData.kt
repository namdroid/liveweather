package com.namdroid.liveweather.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity( tableName = "weather_data" )
data class WeatherData(
        @PrimaryKey
        @ColumnInfo( name = "cityId" )
        var cityId: Int?,

        @ColumnInfo( name = "city_name" )
        var name: String?,

        @ColumnInfo( name = "temp" )
        var temp: Double? = null,

        @ColumnInfo( name = "description" )
        var description: String?,

        @ColumnInfo(name = "humidity")
        var humidity: Float? = null,

        @ColumnInfo( name = "icon" )
        var icon: String?,

        @ColumnInfo( name = "dt" )
        var dt: Long?

) {

    companion object {
        fun mapFrom( weatherResponse: WeatherResponse ) : WeatherData {
            return WeatherData(
                    cityId = weatherResponse.id,
                    name = weatherResponse.name,
                    humidity = weatherResponse.main.humidity,
                    temp = weatherResponse.main.temp,
                    description = weatherResponse.weather[0].description,
                    icon = "http://openweathermap.org/img/w/" +
                            weatherResponse.weather[0].icon + ".png",
                    dt = weatherResponse.dt
            )
        }
    }
}