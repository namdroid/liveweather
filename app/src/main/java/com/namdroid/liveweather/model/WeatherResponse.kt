package com.namdroid.liveweather.model


data class WeatherResponse(
        var id: Int,
        var cod: Int,
        var name: String,
        var message: String,
        var dt: Long,
        var main:Main,
        var weather: List<Weather>
)