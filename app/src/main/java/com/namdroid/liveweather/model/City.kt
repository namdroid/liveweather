package com.namdroid.liveweather.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by namdr on 13.02.2018.
 */
@Entity(tableName = "cities")
data class City(
        @PrimaryKey @ColumnInfo(name = "id")
        val id: Int,
        val name:String?,
        val country:String?,
        @Embedded
        var coord:Coord?
)