package com.namdroid.liveweather.model

/**
 * Created by namdr on 13.02.2018.
 */
data class Coord(var lat:Double?,var lon:Double?)