package com.namdroid.liveweather

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.google.gson.Gson
import com.namdroid.liveweather.model.WeatherData
import com.namdroid.liveweather.model.WeatherResponse
import com.namdroid.liveweather.repository.AppRepository
import com.namdroid.liveweather.ui.detail.DetailViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.nio.charset.Charset

/**
 * Created by namnguyen on 27.02.18.
 */
@RunWith(JUnit4::class)
class DetailViewModelTest {

    @Rule
    @JvmField
    var instantExecutor = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: AppRepository

    @Mock
    lateinit var observer: Observer<WeatherData>

    lateinit var detailViewModel: DetailViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        detailViewModel = DetailViewModel(repository)

    }

    @Test
    fun getWeatherFromRepository() {

        val weatherJson = this.javaClass.classLoader.getResource("weather.json").readText(Charset.forName("UTF-8"))

        val weatherResponse = Gson().fromJson(weatherJson, WeatherResponse::class.java)
        val weatherData:WeatherData = WeatherData.mapFrom(weatherResponse)
        val weatherLiveData = MutableLiveData<WeatherData>()
        weatherLiveData.value = weatherData

        Mockito.`when`(repository.getCityWeather(weatherData.cityId!!)).thenReturn(weatherLiveData)
        detailViewModel.getCityWeather(Mockito.anyInt()).observeForever(observer)

        verify(observer).onChanged(ArgumentMatchers.refEq(weatherData))

    }
}