package com.namdroid.liveweather

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.namdroid.liveweather.model.WeatherResponse
import com.namdroid.liveweather.rest.OpenWeatherService
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset

@RunWith(JUnit4::class)
class WeatherServiceTest {

    @Rule
    @JvmField
    var instantExecutor = InstantTaskExecutorRule()

    private lateinit var weatherService: OpenWeatherService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        weatherService = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/"))
                .client(OkHttpClient())
                .build()
                .create(OpenWeatherService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getWeatherTest() {
        val weatherJson = this.javaClass.classLoader.getResource("weather.json").readText(Charset.forName("UTF-8"))
        mockWebServer.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody(weatherJson))

        val weatherResponse = Gson().fromJson(weatherJson, WeatherResponse::class.java)

        val response = weatherService.getCityWeather(weatherResponse.id,
        Mockito.anyString()).blockingGet()
        Assert.assertNotNull(response)
        Assert.assertEquals(response, weatherResponse)
    }
}