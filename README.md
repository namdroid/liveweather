# Live Weather #

Android Live Weather App

##Tools and Frameworks

* Kotlin
* Arch Components: ViewModel, LiveData, ROOM
* Dagger 2
* RxJava 2
* Picasso
* DataBinding
* Firebase
* Retrofit 2
